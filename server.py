from bottle import route, run, template, static_file, request

@route('/')
def index():
	return template('index')

@route('/hello')
def hello():
	return "Hello World!"

@route('/static/<filename>')
def server_static(filename):
    return static_file(filename, root='/home/gal/Projects/gal')

@route('/tofes')
def tofes():
	return template('tofes')
    
@route('/tofes2')
def show_future():
	friend = request.query.mybf
	grandma = request.query.mygrandma
	bus = request.query.mybus
	animal = request.query.myanimal
	curse = request.query.mycurse
	return template('future', friend=friend, grandma=grandma, bus=bus, animal=animal, curse=curse)

@route('/dibrot')
def dibrot():
	return template('dibrot')

@route('/dibrot', method='POST')
def show_dibra():
	mydibra = request.forms.mydibra
	mylist = ["עדיף כלב שהוא חבר מאשר חבר שהוא כלב", 
			"משנכנס אדר מרבים בשמחה", 
			"מי שטוב לו ושמח כף ימחא", 
			"טובים השניים מן האחד", 
			"מרוב עצים לא רואים את היער",
			"שלום היא מילה שימושית, שלום!", 
			"גל הורסת את החיים", 
			"מקום ראשון רפסודיה", 
			"הכלבים נובחים והשיירה עוברת", 
			"את עשר!"]
	dibra2 = mylist[int(mydibra)-1]
	return template ('dibra', dibra=dibra2)

run(host='localhost', port=8080, debug=True)